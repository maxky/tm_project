const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const ANOTHER = require('../../controllers/anotherWork.controller');
const routes = express.Router();     

module.exports = routes;  
 
routes.use(passport.authenticate('jwt', { session: false }));
routes.get('/', [passport.authenticate('jwt', { session: false })], asyncHandler(ANOTHER.getAnotherWork)); 
// routes.get('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.getUserID)); 
// routes.delete('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.deleteUser)); 
// routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.updateUser));