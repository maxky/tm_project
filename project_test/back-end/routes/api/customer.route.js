const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport');
const customer = require('../../controllers/customer.controller');
const routes = express.Router();

module.exports = routes;

routes.use(passport.authenticate('jwt', { session: false })); 
routes.get('/', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.getCustomer));
routes.get('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.getCustomerID));
routes.get('/tel/:tel', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.getCustomerTel));
routes.get('/account/:account', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.getCustomerAccount));
routes.delete('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.deleteCustomer));
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.updateCustomer));
routes.post('/register', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.insertCustomer));
routes.post('/transferOut', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.transfer));
routes.get('/transferOut/statement', [passport.authenticate('jwt', { session: false })], asyncHandler(customer.getAllTransferOut));