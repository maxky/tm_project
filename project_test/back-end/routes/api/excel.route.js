const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Excel = require('../../controllers/excel.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/register', [passport.authenticate('jwt', { session: false })], asyncHandler(Excel.insertXlsx));  
// routes.get('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.getAllImages));  
// routes.get('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.getImageID)); 
// routes.delete('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.DeleteImage)); 
// routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.UpdateImage));