const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport');
const report = require('../../controllers/report.controller');
const routes = express.Router();

module.exports = routes;

routes.use(passport.authenticate('jwt', { session: false }));  
routes.post('/total', [passport.authenticate('jwt', { session: false })], asyncHandler(report.GetReport)); 
routes.post('/statment/income', [passport.authenticate('jwt', { session: false })], asyncHandler(report.GetReportIncome)); 
routes.post('/statment/expenses', [passport.authenticate('jwt', { session: false })], asyncHandler(report.GetReportExp)); 