const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const PDF = require('../../controllers/pdf.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/register', [passport.authenticate('jwt', { session: false })], asyncHandler(PDF.insertPDF));  
routes.get('/', [passport.authenticate('jwt', { session: false })], asyncHandler(PDF.getAllPDF));  
// routes.get('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.getImageID)); 
// routes.delete('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.DeleteImage)); 
// routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.UpdateImage));