const express = require('express');
const user = require('./user.route'); 
const auth = require('./auth.route'); 
const image = require('./image.route');
const customer = require('./customer.route');
const income = require('./income.route');
const expenses = require('./expenses.route');
const report = require('./report.route');
const excel = require('./excel.route');
const pdf = require('./pdf.route');
const notification = require('./noti.route');
const anotherWork = require('./anotherWork.route');
const routes = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
routes.get('/health-check', (req, res) =>
  res.send('OK')
);
 
routes.use('/user', user);  
routes.use('/auth', auth);   
routes.use('/image', image);
routes.use('/customer', customer);
routes.use('/income', income);
routes.use('/expenses', expenses);
routes.use('/report', report);
routes.use('/excel', excel);
routes.use('/pdf', pdf);
routes.use('/notification', notification);
routes.use('/anotherWork', anotherWork);
module.exports = routes;