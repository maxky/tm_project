const express = require('express'); 
const passport = require('passport');
const asyncHandler = require('express-async-handler')
const auth = require('../../controllers/auth.controller'); 
const user = require('../../controllers/user.controller');
const routes = express.Router();     

module.exports = routes; 
 
routes.post('/register', asyncHandler(user.insertUser), auth.loginUser);   
routes.post('/register/account', asyncHandler(user.insertUserAccount), auth.loginUser);   
routes.post('/login',  [passport.authenticate('local', { session: false })], auth.loginUser);  
routes.get('/token', [passport.authenticate('jwt', { session: false })], auth.loginUser); 