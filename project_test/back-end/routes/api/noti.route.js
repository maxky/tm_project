const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Noti = require('../../controllers/noti.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
// routes.post('/register', [passport.authenticate('jwt', { session: false })], asyncHandler(PDF.insertPDF));  
routes.get('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Noti.getAllNoti));  
// routes.get('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.getImageID)); 
// routes.delete('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Images.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Noti.updateNoti));