const express = require('express');
const userRoutes = require('./users');
const router = express.Router();

router.get('/', function(req, res, next) {
  res.render('login');
});
router.use('/users', userRoutes);


module.exports = router;
