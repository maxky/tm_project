const Response = require("../models/response");
// const https = require("https");
const http = require("http");

// set function name
const messageInsert = "Insert data";
const messageUpdate = "Update data";
const messageDelete = "Delete data";
const messageGet = "Get data";
// set service name
const serviceInsert = "api/service/anotherWork/register";
const serviceUpdate = "api/service/anotherWork/update/";
const serviceDelete = "api/service/anotherWork/delete/";
const serviceGet = "api/service/anotherWork/select";

// PDF object constructor
var ANOTHER = function (another) {
  this.userId = another.userId;
};

const options = {
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization:
      "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjo1OTgsImlhdCI6MTY0MjA1NTY1NSwiZXhwIjoxNjQyMDkxNjU1fQ.kd2z1567GxoqmBhqnTTFSsXr5LuYPKAmhgxLugyHjds",
  },
};

ANOTHER.getAnotherWork = function (userId, customerId, request, result) {
  try {
    let requests = http.request(
      "http://172.21.57.228:1023/api/service/customer/" + customerId,
      options,
      (res) => {
        if (res.statusCode !== 200) {
          Response.buildResponseEntity(
            userId,
            request.requestId,
            request.datetime,
            messageGet,
            serviceGet,
            res.statusCode,
            false,
            res.message,
            null,
            result
          );
          res.resume();
          return;
        } else {
          res.on("data", (data) => {
            Response.buildResponseEntity(
              userId,
              request.requestId,
              request.datetime,
              messageGet,
              serviceGet,
              res.statusCode,
              true,
              "get data successfully",
              JSON.parse(data).length > 0
                ? JSON.parse(data)
                : "can't found data for this Id: " + customerId,
              result
            );
          });
        }
      }
    );
    requests.on("error", (error) => {
      Response.buildResponseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageGet,
        serviceGet,
        400,
        false,
        "Connection Error",
        error,
        result
      );
    });
    requests.end();
  } catch (error) {
    Response.buildResponseEntity(
      userId,
      request.requestId,
      request.datetime,
      messageGet,
      serviceGet,
      500,
      false,
      error,
      null,
      result
    );
  }
};

module.exports = ANOTHER;

//   var jsonParsedArray = JSON.parse(data);
//   for (key in jsonParsedArray) {
//     console.log(jsonParsedArray[0].id);
//     console.log(jsonParsedArray[0].name);
//     console.log(jsonParsedArray[0].account);
//     console.log(jsonParsedArray[0].account_type);
//     console.log(jsonParsedArray[0].total);
//     console.log(jsonParsedArray[0].village);
//     console.log(jsonParsedArray[0].districk);
//     console.log(jsonParsedArray[0].province);
//     console.log(jsonParsedArray[0].tel);
//     console.log(jsonParsedArray[0].cif);
//     console.log(jsonParsedArray[0].currency);
//     console.log(jsonParsedArray[0].currency_letter);
//     console.log(jsonParsedArray[0].total_letter);
//   }
