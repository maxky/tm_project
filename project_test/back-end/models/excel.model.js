var sql = require("../config/connectdb");
const readXlsxFile = require("read-excel-file/node"); 
const Response = require("../models/response");
// set data
const messageErrorFile = "This file isn't Excel file.";
// set function name 
const messageInsert = "Insert data";
const messageUpdate = "Update data";
const messageDelete = "Delete data";
const messageGet = "Get data";
// set service name
const serviceInsert = "api/service/excel/register";
const serviceUpdate = "api/service/excel/update/";
const serviceDelete = "api/service/excel/delete/";
const serviceGet = "api/service/excel/";

//set function for add log and response data
async function responseEntity(
  userId,
  requestId,
  datetime,
  functionName,
  serviceName,
  code,
  status,
  message,
  description,
  result
) {
  Response.buildResponseEntity(
    userId,
    requestId,
    datetime,
    functionName,
    serviceName,
    code,
    status,
    message,
    description,
    result
  );
}

// Images object constructor
var Excel = function (xlsx) {
  this.id = xlsx.id;
  this.firstname = xlsx.firstname;
  this.lastname = xlsx.lastname;
  this.tel = xlsx.tel;
};

Excel.insertXlsx = function (
  filename,
  userId,
  request,
  errorData,
  filetype,
  result
) {
  try {
    if (errorData)
      responseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageInsert,
        serviceInsert,
        errorData.statusCode,
        false,
        errorData,
        null,
        result
      );

    if (!filetype) {
      responseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageInsert,
        serviceInsert,
        204,
        false,
        messageErrorFile,
        null,
        result
      );
    } else {
      readXlsxFile(filename).then((rows) => {
        if (JSON.stringify(rows[0]) === '["Firstname","Lastname","Tel"]') {
          rows.shift();
          let setData = [];

          rows.forEach((row) => {
            let tutorial = [row[0], row[1], row[2], request.requestId];
            setData.push(tutorial);
          });
          sql.query(
            "INSERT INTO tbexcel (firstname, lastname, tel, reqid) VALUES ?",
            [setData],
            function (err, res) {
              responseEntity(
                userId,
                request.requestId,
                request.datetime,
                messageInsert,
                serviceInsert,
                res.statusCode,
                res.protocol41,
                "Register successfully",
                filename,
                result
              );
            }
          );
        } else {
          responseEntity(
            userId,
            request.requestId,
            request.datetime,
            false,
            204,
            "Excel format is incorrect",
            null,
            result
          );
        }
      });
    }
  } catch (error) {
    responseEntity(
      userId,
      request.requestId,
      request.datetime,
      messageInsert,
      serviceInsert,
      500,
      false,
      error,
      null,
      result
    );
  }
};

module.exports = Excel;
