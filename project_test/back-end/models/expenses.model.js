var sql = require("../config/connectdb");
const Response = require("../models/response");
// set function name
const messageInsert = "Insert data";
const messageUpdate = "Update data";
const messageDelete = "Delete data";
const messageGet = "Get data";
// set service name
const serviceInsert = "api/service/expenses/register";
const serviceUpdate = "api/service/expenses/update/";
const serviceDelete = "api/service/expenses/delete/";
const serviceGet = "api/service/expenses/";
//set function for add log and response data
async function responseEntity(
  userId,
  requestId,
  datetime,
  functionName,
  serviceName,
  code,
  status,
  message,
  description,
  result
) {
  Response.buildResponseEntity(
    userId,
    requestId,
    datetime,
    functionName,
    serviceName,
    code,
    status,
    message,
    description,
    result
  );
}

//Task object constructor
var Expenses = function (income) {
  this.amount = income.amount;
  this.description = income.description;
  this.datetime = income.datetime;
  this.id = income.id;
};

Expenses.insertExpenses = function (req, result) {
  try {
    sql.query(
      "INSERT INTO `expenses` (amount, description, datetime, CusId) VALUE (?,?,?,?) ",
      [req.amount, req.description, req.datetime, req.id],
      function (err, res) {
        if (err) throw err;
        responseEntity(
          req.id,
          Date.now(),
          req.datetime,
          messageInsert,
          serviceInsert,
          res.statusCode,
          res.protocol41,
          "Register successfully",
          req,
          result
        );
      }
    );
  } catch (error) {
    responseEntity(
      req.id,
      Date.now(),
      req.datetime,
      messageInsert,
      serviceInsert,
      500,
      false,
      error,
      null,
      result
    );
  }
};

module.exports = Expenses;
