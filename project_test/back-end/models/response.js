var sql = require("../config/connectdb");
let requestId = null;
let datetime = null;
async function getRequestDatetime() {
  let date_ob = new Date();
  let date = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();
  requestId = year + month + date + hours + minutes + seconds;
  datetime =
    year +
    "-" +
    month +
    "-" +
    date +
    " " +
    hours +
    ":" +
    minutes +
    ":" +
    seconds;
}

// Images object constructor
var Response = function (response) {
  getRequestDatetime();
  if (requestId && datetime) {
    this.userId = response.userId;
    this.requestId = requestId;
    this.success = response.success;
    this.datetime = datetime;
    this.message = response.message;
    this.data = response.data;
  }
};

async function InsertLog(
  userId,
  requestId,
  datetime,
  functionName,
  serviceName,
  code,
  status,
  message,
  description
) {
  if (
    serviceName !== "api/service/notification/" ||
    functionName !== "Get data"
  ) {
    sql.query(
      "INSERT INTO tblogs (userId, requestId, datetime, functionName, serviceName, code, status, message, description) VALUES (?,?,?,?,?,?,?,?,?)",
      [
        userId,
        requestId,
        datetime,
        functionName,
        serviceName,
        code,
        status,
        message,
        description,
      ]
    );
  }
  if (
    serviceName === "api/service/expenses/register" ||
    serviceName === "api/service/income/register"
  ) {
    sql.query(
      "INSERT INTO `tbnotification` (reference, datetime, status, functionName, data) VALUE (?,?,?,?,?) ",
      [requestId, datetime, "No", serviceName, description]
    );
  }

  return;
}

async function responseMessage(
  requestId,
  status,
  datetime,
  message,
  data,
  result
) {
  return result(null, {
    reqId: requestId,
    success: status,
    datetime: datetime,
    message: message,
    data: data,
  });
}

Response.buildResponseEntity = function (
  userId,
  requestId,
  datetime,
  functionName,
  serviceName,
  code,
  status,
  message,
  description,
  result
) {
  InsertLog(
    userId,
    requestId,
    datetime,
    functionName,
    serviceName,
    code,
    status === true ? "success" : "error",
    message,
    JSON.stringify(description)
  );
  responseMessage(requestId, status, datetime, message, description, result);
};

module.exports = Response;
