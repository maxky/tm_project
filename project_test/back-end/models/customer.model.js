var sql = require("../config/connectdb");
const sqlQuery = require("../config/sqlQuery");
const Response = require("../models/response");

// set function name
const messageInsert = "Insert data";
const messageUpdate = "Update data";
const messageDelete = "Delete data";
const messageGet = "Get data";
// set service name
const serviceInsert = "api/service/customer/register";
const serviceUpdate = "api/service/customer/update/";
const serviceDelete = "api/service/customer/delete/";
const serviceGet = "api/service/customer/select";

//Task object constructor
var Customer = function (NAME) {
  this.name = NAME.name;
  this.tel = NAME.tel;
  this.transferOut = NAME.transferOut;
  this.amount = NAME.amount;
  this.transferIn = NAME.transferIn;
};

Customer.getAllCustomer = function (request, result) {
  // sql.query("select * from customer", function (err, res) {
  // sql.query("SELECT id, name FROM `customer`", function (err, res) {
  sql.query(sqlQuery.get_customer, function (err, res) {
    try {
      if (err) {
        Response.buildResponseEntity(
          null,
          request.requestId,
          request.datetime,
          messageGet,
          serviceGet,
          204,
          false,
          err.message,
          null,
          result
        );
      } else {
        Response.buildResponseEntity(
          null,
          request.requestId,
          request.datetime,
          messageGet,
          serviceGet,
          200,
          true,
          "get data successfully",
          res,
          result
        );
      }
    } catch (error) {
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        messageGet,
        serviceGet,
        500,
        false,
        error,
        null,
        result
      );
    }
  });
};

Customer.getCustomerID = function (id, result) {
  // sql.query("select * from customer where id= ?", id, function (err, res) {
  sql.query(sqlQuery.get_customerID, id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Customer.getCustomerTel = function (tel, result) {
  // sql.query("select * from customer where id= ?", id, function (err, res) {
  sql.query("select * from customer where tel = ?", tel, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      if (res.length > 0) {
        result(null, {
          success: true,
          message: "Customer detail",
          data: res[0],
        });
      } else {
        result(null, {
          success: false,
          message: "Token true and Try incloud Tel again",
        });
      }
    }
  });
};

Customer.getCustomerAccount = function (account, result) {
  // sql.query("select * from customer where id= ?", id, function (err, res) {
  sql.query(
    "select * from customer where account = ?",
    account,
    function (err, res) {
      if (err) {
        result(null, err);
      } else {
        if (res.length > 0) {
          result(null, {
            success: true,
            message: "Customer detail",
            data: res[0],
          });
        } else {
          result(null, {
            success: false,
            message: "Token true and Try incloud Account again",
          });
        }
      }
    }
  );
};

Customer.insertCustomer = function (newUser, result) {
  // sql.query("insert into customer (name) values (?)", [newUser.name], function (
  sql.query(sqlQuery.insert_customer, [newUser.name], function (err, res) {
    if (err) throw err;
    result(null, {
      success: true,
      message: "Register successfully",
      data: [newUser],
    });
  });
};

Customer.deleteCustomer = function (id, result) {
  // sql.query("DELETE FROM customer WHERE id = ?", id, function (err, res) {
  sql.query(sqlQuery.delete_customer, id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, {
        success: true,
        message: "Delete data successfully",
        customerID: id,
      });
    }
  });
};

Customer.updateCustomer = function (id, Customer, result) {
  // sql.query("UPDATE customer set ? where id= " + id, Customer, function (err, res) {
  sql.query(sqlQuery.update_customer + id, Customer, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, {
        success: true,
        message: "Update data successfully",
        customerID: id,
        customer: [Customer],
      });
    }
  });
};

Customer.transfer = function (transferOut, amount, transferIn, result) {
  sql.query(
    "SELECT account, total, name, account_type, currency, tel from customer where account= " +
      transferOut,
    function (err, resOut) {
      try {
        if (err) {
          result(null, err);
        } else {
          if (amount > resOut[0].total - 10000) {
            result(null, {
              success: false,
              message: "transferOut revert",
              resutl: resOut,
            });
          } else {
            let Debit = resOut[0].total - amount;
            sql.query(
              "UPDATE customer set total = ? WHERE account = " + transferOut,
              Debit,
              function (err, resDebit) {
                if (err) {
                  result(null, err);
                } else {
                  sql.query(
                    "SELECT account, total, name, account_type, currency, tel from customer where account= " +
                      transferIn,
                    function (err, resIn) {
                      if (err) {
                        result(null, err);
                      } else {
                        let Crebit = resIn[0].total + amount;
                        sql.query(
                          "UPDATE customer set total = ? WHERE account = " +
                            transferIn,
                          Crebit,
                          function (err, resCrebit) {
                            if (err) {
                              result(null, err);
                            } else {
                              sql.query(
                                "INSERT INTO transferout (transferout, amount, transferin) VALUES ('" +
                                  transferOut +
                                  "', '" +
                                  amount +
                                  "', '" +
                                  transferIn +
                                  "')",
                                function (err, resInsertTransfer) {
                                  if (err) {
                                    result(null, err);
                                  } else {
                                    let date_ob = new Date();
                                    let date = ("0" + date_ob.getDate()).slice(
                                      -2
                                    );
                                    let month = (
                                      "0" +
                                      (date_ob.getMonth() + 1)
                                    ).slice(-2);
                                    let year = date_ob.getFullYear();
                                    result(null, {
                                      success: true,
                                      message: "transferOut successfully",
                                      datetime: date + "-" + month + "-" + year,
                                      transferOut: resOut[0],
                                      amount: amount,
                                      transferIn: resIn[0],
                                    });
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              }
            );
          }
        }
      } catch (error) {
        throw error(err);
      }
    }
  );
};

Customer.getAllTransferOut = function (result) {
  sql.query("SELECT * FROM transferout", function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, { data: res });
    }
  });
};

module.exports = Customer;
