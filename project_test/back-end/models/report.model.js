var sql = require("../config/connectdb");
//Task object constructor
var Report = function (income) {
  this.starttime = income.starttime;
  this.endtime = income.endtime;
  this.id = income.id;
};

Report.GetReport = function (req, result) {
  sql.query(
    "select sum(amount) total from income where DATE(datetime) >= '" +
      req.starttime +
      "' AND DATE(datetime) <= '" +
      req.endtime +
      "' AND CusId = '" +
      req.id +
      "';",
    function (err, resInc) {
      if (err) {
        result(null, err);
      } else {
        if (resInc.length > 0) {
          sql.query(
            "select sum(amount) total from expenses where DATE(datetime) >= '" +
              req.starttime +
              "' AND DATE(datetime) <= '" +
              req.endtime +
              "' AND CusId = '" +
              req.id +
              "';",
            function (err, resExp) {
              if (err) {
                result(null, err);
              } else {
                if (resExp.length > 0) {
                  let date_ob = new Date();
                  let date = ("0" + date_ob.getDate()).slice(-2);
                  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
                  let year = date_ob.getFullYear();
                  let hours = date_ob.getHours();
                  let minutes = date_ob.getMinutes();
                  let seconds = date_ob.getSeconds();
                  let Balance = resInc[0].total - resExp[0].total;
                  result(null, {
                    success: true,
                    message: "total payment startdate to enddate",
                    income: resInc[0].total,
                    expenses: resExp[0].total,
                    total: Balance,
                  });
                  console.log("====================================");
                  console.log(
                    "total" +
                      "|datetime:" +
                      date +
                      "-" +
                      month +
                      "-" +
                      year +
                      " " +
                      hours +
                      ":" +
                      minutes +
                      ":" +
                      seconds +
                      "|id:" +
                      req.id
                  );
                } else {
                  result(null, {
                    success: false,
                    message: "please select expenses date",
                  });
                }
              }
            }
          );
        } else {
          result(null, {
            success: false,
            message: "please select income date",
          });
        }
      }
    }
  );
};

Report.GetReportIncome = function (req, result) {
  sql.query(
    "select * from income where DATE(datetime) >= '" +
      req.starttime +
      "' AND DATE(datetime) <= '" +
      req.endtime +
      "' AND CusId = '" +
      req.id +
      "' ORDER BY datetime ASC;",
    function (err, resInc) {
      if (err) {
        result(null, err);
      } else {
        if (resInc.length > 0) {
          let date_ob = new Date();
          let date = ("0" + date_ob.getDate()).slice(-2);
          let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
          let year = date_ob.getFullYear();
          let hours = date_ob.getHours();
          let minutes = date_ob.getMinutes();
          let seconds = date_ob.getSeconds();
          result(null, {
            data: resInc,
          });
          console.log("====================================");
          console.log(
            "income" +
              "|datetime:" +
              date +
              "-" +
              month +
              "-" +
              year +
              " " +
              hours +
              ":" +
              minutes +
              ":" +
              seconds +
              "|id:" +
              req.id
          );
        } else {
          result(null, {
            data: [] 
          });
        }
      }
    }
  );
};

Report.GetReportExp = function (req, result) {
  sql.query(
    "select * from expenses where DATE(datetime) >= '" +
      req.starttime +
      "' AND DATE(datetime) <= '" +
      req.endtime +
      "' AND CusId = '" +
      req.id +
      "'ORDER BY datetime ASC;",
    function (err, resExp) {
      if (err) {
        result(null, err);
      } else {
        if (resExp.length > 0) {
          let date_ob = new Date();
          let date = ("0" + date_ob.getDate()).slice(-2);
          let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
          let year = date_ob.getFullYear();
          let hours = date_ob.getHours();
          let minutes = date_ob.getMinutes();
          let seconds = date_ob.getSeconds();
          result(null, {
            data: resExp,
          });
          console.log("====================================");
          console.log(
            "expenses" +
              "|datetime:" +
              date +
              "-" +
              month +
              "-" +
              year +
              " " +
              hours +
              ":" +
              minutes +
              ":" +
              seconds +
              "|id:" +
              req.id
          );
        } else {
          result(null, {
            data: [],
          });
        }
      }
    }
  );
};

module.exports = Report;
