var sql = require("../config/connectdb");
const Response = require("../models/response");
// set data
const messageErrorFile = "This file isn't PDF file.";
// set function name 
const messageInsert = "Insert data";
const messageUpdate = "Update data";
const messageDelete = "Delete data";
const messageGet = "Get data";
// set service name
const serviceInsert = "api/service/pdf/register";
const serviceUpdate = "api/service/pdf/update/";
const serviceDelete = "api/service/pdf/delete/";
const serviceGet = "api/service/pdf/";
//set function for add log and response data
async function responseEntity(
  userId,
  requestId,
  datetime,
  functionName,
  serviceName,
  code,
  status,
  message,
  description,
  result
) {
  Response.buildResponseEntity(
    userId,
    requestId,
    datetime,
    functionName,
    serviceName,
    code,
    status,
    message,
    description,
    result
  );
}

// PDF object constructor
var PDF = function (xlsx) {
  this.id = xlsx.id;
  this.filename = xlsx.filename;
};

PDF.insertPDF = function (
  filename,
  userId,
  request,
  errorData,
  filetype,
  result
) {
  try {
    if (errorData)
      responseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageInsert,
        serviceInsert,
        errorData.statusCode,
        false,
        errorData,
        null,
        result
      );

    if (!filetype) {
      responseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageInsert,
        serviceInsert,
        false,
        messageErrorFile,
        null,
        result
      );
    } else {
      sql.query(
        "INSERT INTO tbpdf (filename, reqid) VALUES (?,?)",
        [filename, request.requestId],
        function (err, res) {
          responseEntity(
            userId,
            request.requestId,
            request.datetime,
            messageInsert,
            serviceInsert,
            res.statusCode,
            res.protocol41,
            "Register successfully",
            filename,
            result
          );
        }
      );
    }
  } catch (error) {
    responseEntity(
      userId,
      request.requestId,
      request.datetime,
      messageInsert,
      serviceInsert,
      error.statusCode,
      false,
      error,
      null,
      result
    );
  }
};

PDF.getAllPDF = function (userId, request, result) {
  try {
    sql.query("select * from tbpdf", function (err, res) { 
      responseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageGet,
        serviceGet,
        200,
        true,
        "get data successfully",
        res,
        result
      );
    });
  } catch (error) {
    responseEntity(
      userId,
      request.requestId,
      request.datetime,
      messageGet,
      serviceGet,
      error.statusCode,
      false,
      error,
      null,
      result
    );
  }
};

module.exports = PDF;
