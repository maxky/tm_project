var sql = require("../config/connectdb");
var bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
const config = require("../config/config");
const sqlQuery = require("../config/sqlQuery");
const nimberRount = 10;
const salt = bcrypt.genSaltSync(nimberRount);
//Task object constructor
var Users = function (users) {
  this.id = users.id;
  this.username = users.username;
  this.email = users.email;
  this.password = bcrypt.hashSync(users.password, salt);
  this.role = users.role;
};

Users.getAllUsers = function (result) {
  // sql.query("select * from users", function (err, res) {
  // sql.query("call pcd_users()", function (err, res) {
  sql.query(sqlQuery.get_user, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, res[0]);
    }
  });
};

Users.getUserID = function (id, result) {
  // sql.query("select * from users whrere id= ?", id, function (err, res) {
  // sql.query("call pcd_userId(?)", id, function (err, res) {
  sql.query(sqlQuery.get_userID, id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, res[0]);
    }
  });
};

Users.insertUser = function (newUser, result) {
  if (newUser.username === newUser.email) {
    result(null, {
      success: false,
      message: "please change useranem and passowrd",
    });
  } else {
    // sql.query("insert into users (email,username,password) values (?,?,?)", [newUser.email, newUser.username, newUser.password], function (err, res) {
    // sql.query("call pcd_userInsert(?,?,?)", [newUser.email, newUser.username, newUser.password], function (err, res) {
    sql.query(
      sqlQuery.insert_user,
      [newUser.email, newUser.username, newUser.password],
      function (err, res) {
        if (err) throw err;
        result(null, {
          success: true,
          message: "Register successfully",
          data: [newUser],
        });
      }
    );
  }
};

Users.insertUserAccount = function (newUser, result) {
  if (newUser.username === newUser.email) {
    result(null, {
      success: false,
      message: "please change useranem and passowrd",
    });
  } else {
    sql.query(
      "insert into users (email,username, password, role) values (?,?,?,?)",
      [newUser.email, newUser.username, newUser.password, "account"],
      function (err, res) {
        if (err) throw err;
        result(null, {
          success: true,
          message: "Register successfully",
          data: [newUser],
        });
      }
    );
  }
};

Users.deleteUser = function (id, result) {
  // sql.query("DELETE FROM users WHERE id = ?", id, function (err, res) {
  // sql.query("call pcd_userDelete(?)", id, function (err, res) {
  sql.query(sqlQuery.delete_user, id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, {
        success: true,
        message: "Delete data successfully",
        userID: id,
      });
    }
  });
};

Users.updateUser = function (id, newUser, result) {
  // sql.query("UPDATE users set ? where id= " + id, newUser, function (err, res) {
  // sql.query("call pcd_userUpdate(?,?,?,?)", [id, newUser.email, newUser.username, newUser.password], function (err, res) {
  sql.query(
    sqlQuery.update_user,
    [id, newUser.email, newUser.username, newUser.password],
    function (err, res) {
      if (err) {
        result(null, err);
      } else {
        result(null, {
          success: true,
          message: "Update data successfully",
          userID: id,
          users: [newUser],
        });
      }
    }
  );
};

Users.loginUser = function (data, result) {
  try {
    sql.query(
      "SELECT id, role, username, status FROM users WHERE username = '" +
        [data.username] +
        "';",
      function (err, res) {
        if (err) {
          result(null, err);
        } else {
          if (res[0].id > 0) {
            var token = jwt.sign({ data: res[0].id }, config.jwtsecret, {
              expiresIn: config.time,
              // expiresIn: 10,
            });
            let date_ob = new Date();
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            let year = date_ob.getFullYear();
            let hours = date_ob.getHours();
            let minutes = date_ob.getMinutes();
            let seconds = date_ob.getSeconds();
            result(null, {
              success: true,
              token: token,
              message: "Login successfully",
              username: res[0].username,
              loginID: res[0].id,
              profile: res[0].role,
              status: res[0].status,
              datetime:
                date +
                "-" +
                month +
                "-" +
                year +
                " " +
                hours +
                ":" +
                minutes +
                ":" +
                seconds,
            });
            console.log("====================================");
            console.log(
              "login" +
                "|datetime:" +
                date +
                "-" +
                month +
                "-" +
                year +
                " " +
                hours +
                ":" +
                minutes +
                ":" +
                seconds +
                "|username:" +
                res[0].username +
                "|id:" +
                res[0].id
            );
          } else {
            result(null, { success: false, message: "please try again" });
          }
        }
      }
    );
  } catch (error) {
    result(null, { message: error });
  }
};

Users.lockUser = function (id, result) {
  sql.query("UPDATE users set status = 'lock' where id = ?;", id, function (err, res) {
    // sql.query("call pcd_userDelete(?)", id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, {
        success: true,
        message: "Lock data successfully",
        userID: id,
      });
    }
  });
};

Users.activeUser = function (id, result) {
  sql.query("UPDATE users set status = 'active' where id = ?;", id, function (err, res) {
    // sql.query("call pcd_userDelete(?)", id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, {
        success: true,
        message: "Active data successfully",
        userID: id,
      });
    }
  });
};

module.exports = Users;
