var sql = require("../config/connectdb");
const Response = require("../models/response");
// set function name
const messageInsert = "Insert data";
const messageUpdate = "Update data";
const messageDelete = "Delete data";
const messageGet = "Get data";
// set service name
const serviceInsert = "api/service/notification/register";
const serviceUpdate = "api/service/notification/update/";
const serviceDelete = "api/service/notification/delete/";
const serviceGet = "api/service/notification/";
//set function for add log and response data
async function responseEntity(
  userId,
  requestId,
  datetime,
  functionName,
  serviceName,
  code,
  status,
  message,
  description,
  result
) {
  Response.buildResponseEntity(
    userId,
    requestId,
    datetime,
    functionName,
    serviceName,
    code,
    status,
    message,
    description,
    result
  );
}

// PDF object constructor
var Noti = function (noti) {
  this.id = noti.id;
  this.status = noti.status;
};

Noti.updateNoti = function (id, userId, request, result) {
  try {
    if (!id) {
      responseEntity(
        userId,
        request.requestId,
        request.datetime,
        messageUpdate,
        serviceUpdate,
        204,
        false,
        "This Id is problem.",
        null,
        result
      );
    } else {
      sql.query(
        "update tbnotification set status = ? where id =" + id,
        ["Yes", id],
        function (err, res) {
          responseEntity(
            userId,
            request.requestId,
            request.datetime,
            messageUpdate,
            serviceUpdate,
            res.statusCode,
            res.protocol41,
            "Update data successfully",
            id,
            result
          );
        }
      );
    }
  } catch (error) {
    responseEntity(
      userId,
      request.requestId,
      request.datetime,
      messageUpdate,
      serviceUpdate,
      error.statusCode,
      false,
      error,
      null,
      result
    );
  }
};

Noti.getAllNoti = function (userId, request, result) {
  try {
    sql.query(
      "select * from tbnotification where status = 'No' ",
      function (err, res) { 
        responseEntity(
          userId,
          request.requestId,
          request.datetime,
          messageGet,
          serviceGet,
          res.statusCode,
          true,
          "get data successfully",
          res,
          result
        );
      }
    );
  } catch (error) {
    responseEntity(
      userId,
      request.requestId,
      request.datetime,
      messageGet,
      serviceGet,
      error.statusCode,
      false,
      error,
      null,
      result
    );
  }
};

module.exports = Noti;
