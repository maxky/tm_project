var sql = require("../config/connectdb");
const sqlQuery = require("../config/sqlQuery");
// Images object constructor
var Images = function (img) {
  this.image = img.image;
  this.imageName = img.imageName;
};

Images.insertImage = function (filename, value, result) {
  sql.query(
    // "insert into image (image, imageName) values (?,?)",
    sqlQuery.insert_image,
    [filename, value],
    function (err, res) {
      if (err) {
        return result(null, {
          success: false,
          message: err,
        });
      } else {
        return result(null, {
          success: true,
          message: "Register successfully",
          imageID: [filename, value],
        });
      }
    }
  );
};

Images.getAllImages = function (result) {
  // sql.query("select * from image", function (err, res) {
  sql.query(sqlQuery.get_image, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Images.getImageID = function (id, result) {
  // sql.query("select * from image where id= ?", id, function (err, res) {
  sql.query(sqlQuery.get_imageID, id, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Images.DeleteImage = function (id, result) {
  // sql.query("delete from image where id= ?", id, function (err, res) {
  sql.query(sqlQuery.delete_image, id, function (err, res) {
    if (err) {
      return result(null, {
        success: false,
        message: err,
      });
    } else {
      return result(null, {
        success: true,
        message: "Delete data successfully",
        imageID: id,
      });
    }
  });
};

Images.UpdateImage = function (id, filename, value, result) {
  sql.query(
    // "update image set image = ?, imageName = ? where id =" + id,
    sqlQuery.update_image + id,
    [filename, value],
    function (err, res) {
      if (err) {
        return result(null, {
          success: false,
          message: err,
        });
      } else {
        return result(null, {
          success: true,
          message: "Update data successfully",
          imageID: id,
          image: [filename, value],
        });
      }
    }
  );
};

module.exports = Images;
