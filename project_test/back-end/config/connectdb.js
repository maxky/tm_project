var mysql = require('mysql');
var config = require('./config') 

const connection = {
    host     : `localhost`,
    user     : `root`,
    password : ``,
    database : `fullstack_uat`,
    charset: 'utf8_general_ci',
    typeCast: function (field, next) {
        if (field.type == 'JSON') {
            return (JSON.parse(field.string()));
        }
        return next();
    }
} 

module.exports = mysql.createConnection(connection);