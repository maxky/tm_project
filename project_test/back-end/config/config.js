const assert = require( "assert" ); 
const dotenv = require( "dotenv" );

// read in the .env file 
dotenv.config();

// capture the environment variables the application needs

const { 
    NODE_ENV, 
    PORT,
    HOST,
    HOST_URL,
    JWTSECRET,
    SECRET,
    TIME,
    HOSTNAME,
    USERNAMEDB,
    PASSWORD,
    DATABASENAME 
} = process.env;

const sqlEncrypt = process.env.SQL_ENCRYPT === "true";

// validate the required configuration information 
assert(PORT, "PORT configuration is required." ); 
assert(NODE_ENV, "NODE_ENV configuration is required." ); 
// export the configuration information

module.exports = { 
    env: NODE_ENV,
    port: PORT,
    host: HOST,
    host_url: HOST_URL,
    jwtsecret: JWTSECRET,
    secret: SECRET,
    time: TIME,
    hostname: HOSTNAME,
    usernameDB: USERNAMEDB,
    password: PASSWORD,
    databasename: DATABASENAME
};