const passport = require("passport");
const LocalStrategy = require("passport-local");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
var bcrypt = require("bcrypt");
var sql = require("../config/connectdb");
const config = require("../config/config");

const localLogin = new LocalStrategy(
  {
    usernameField: "username",
  },
  function (username, password, done) {
    sql.query(
      "select * from users where username='" + username + "' ",
      function (err, user) {
        try {
            // console.log(bcrypt.compareSync(password, user[0].password));
            // console.log(password + " " + user[0].password);
          // if (!user || !bcrypt.compareSync(password, user.hashedPassword)) {
          if (!user || !bcrypt.compareSync(password, user[0].password)) {
            return done(null, false, { message: "Incorrect user name" });
          }
          // if (user[0].role != 'account') {
          //   return done(null, false, { error: 'Your account is role!' });
          // }
          return done(null, user);
        } catch (error) {
          return done(null, err);
        }
      }
    );
  }
);

const jwtLogin = new JwtStrategy(
  {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtsecret,
  },
  function (username, done) {
    sql.query(
      "select * from users where username='" + username + "' ",
      function (err, user) {
        try {
          if (err) {
            return done(null, err);
          }
          if (!user) {
            return done(null, false);
          }
          return done(null, true, user);
        } catch (error) {
          return done(null, err);
        }
      }
    );
  }
);

passport.use(jwtLogin);
passport.use(localLogin);

module.exports = passport;
