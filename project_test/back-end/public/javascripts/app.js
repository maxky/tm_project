function login() {
  $.ajax({
    type: "POST",
    url: '/api/service/auth/login',
    data: {username:$('#username').val(),password:$('#password').val()},
    success: function(data) {
      // console.log(data) 
      if (data.token) {
        localStorage.setItem('token', data.token);
        window.location.replace("/users");
      }
    },
    error: onRequestError,
  });
  return false;
}
function logout() {
  localStorage.removeItem('token');
  window.location.replace("/");
}

function onRequestError() {
  logout()
}

function reload() {
  location.reload();
}
