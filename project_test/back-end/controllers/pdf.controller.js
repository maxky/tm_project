const PDF = require("../models/pdf.model");
var multer = require("multer");
const Response = require("../models/response");
var __basedir = "upload/pdf/";
// set type of file
let filetype = false;

const myStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __basedir);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "_" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "application/pdf") {
    filetype = true;
    cb(null, true);
  } else {
    filetype = false;
    cb(null, false);
  }
};

const upload = multer({
  storage: myStorage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
}).single("uploadfile");

exports.insertPDF = function (req, res) {
  var request = new Response(req.body);
  upload(req, res, function (error) {
    PDF.insertPDF(
      filetype ? req.file.filename : null,
      req.body.userId,
      request,
      error,
      filetype,
      function (err, pdf) {
        if (err) res.send(null, err);
        res.json(pdf);
      }
    );
  });
};

exports.getAllPDF = function (req, res) {
  var request = new Response(req.body);
  PDF.getAllPDF(req.body.userId, request, function (err, pdf) {
    if (err) res.send(err);
    res.json(pdf);
  }); 
};
