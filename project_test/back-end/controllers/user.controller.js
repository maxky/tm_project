var Users = require("../models/user.model");

exports.getUsers = function (req, res) {
  Users.getAllUsers(function (err, user) {
    if (err) res.send(err);
    res.json(user);
  });
};

exports.getUserID = function (req, res) {
  Users.getUserID(req.params.id, function (err, user) {
    if (err) res.send(err);
    res.json(user);
  });
};

exports.insertUser = function (req, res) {
  var new_user = new Users(req.body);

  //handles null error
  if (!new_user.username || !new_user.email) {
    res
      .status(400)
      .send({ error: false, message: "Please provide users/status" });
  } else {
    Users.insertUser(new_user, function (err, user) {
      if (err) res.send(err);
      res.json(user);
    });
  }
};

exports.insertUserAccount = function (req, res) {
  var new_user = new Users(req.body);

  //handles null error
  if (!new_user.username || !new_user.email) {
    res
      .status(400)
      .send({ error: false, message: "Please provide users/status" });
  } else {
    Users.insertUserAccount(new_user, function (err, user) {
      if (err) res.send(err);
      res.json(user);
    });
  }
};

exports.deleteUser = function (req, res) {
  Users.deleteUser(req.params.id, function (err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};

exports.updateUser = function (req, res) {
  var newuser = new Users(req.body);

  Users.updateUser(req.params.id, newuser, function (err, user) {
    if (err) res.send(err);
    res.json(user);
  });
};

exports.lockUser = function (req, res) {
  Users.lockUser(req.params.id, function (err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};

exports.activeUser = function (req, res) {
  Users.activeUser(req.params.id, function (err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};
