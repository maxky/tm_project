var Report = require("../models/report.model");

exports.GetReport = function (req, res) {
  var report = new Report(req.body);
  Report.GetReport(report, function (err, report) {
    if (err) res.send(err);
    res.json(report);
  });
};

exports.GetReportIncome = function (req, res) {
  var report = new Report(req.body);
  Report.GetReportIncome(report, function (err, report) {
    if (err) res.send(err);
    res.json(report);
  });
};

exports.GetReportExp = function (req, res) {
  var report = new Report(req.body);
  Report.GetReportExp(report, function (err, report) {
    if (err) res.send(err);
    res.json(report);
  });
};
