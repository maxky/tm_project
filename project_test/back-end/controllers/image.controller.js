const Images = require("../models/image.model");
var multer = require("multer");

const myStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "upload/image/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: myStorage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
}).single("picturefile");

exports.insertImage = function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.end("Error uploading file.");
    }
    Images.insertImage(req.file.filename, req.body.imageName, function (
      err,
      image
    ) {
      if (err) res.send(null, err);
      res.json(image);
    });
  });
};

exports.getAllImages = function (req, res) {
  Images.getAllImages(function (err, image) {
    if (err) res.send(err);
    res.json(image);
  });
};

exports.getImageID = function (req, res) {
  Images.getImageID(req.params.id, function (err, image) {
    if (err) res.send(err);
    res.json(image);
  });
};

exports.DeleteImage = function (req, res) {
  Images.DeleteImage(req.params.id, function (err, image) {
    if (err) res.send(err);
    res.json(image);
  });
};

exports.UpdateImage = function (req, res) { 
  upload(req, res, function (err) {
    if (err) {
      return res.end("Error uploading file.");
    }
    Images.UpdateImage(req.params.id, req.file.filename, req.body.imageName, function (
      err,
      image
    ) {
      if (err) res.send(err);
      res.json(image);
    });
  });
};
