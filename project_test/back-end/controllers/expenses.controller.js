var Expenses = require("../models/expenses.model");

exports.insertExpenses = function (req, res) {
  var expenses = new Expenses(req.body);
  Expenses.insertExpenses(expenses, function (err, expenses) {
    if (err) res.send(err);
    res.json(expenses);
  });
};
