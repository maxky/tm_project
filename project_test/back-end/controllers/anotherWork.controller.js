const ANOTHER = require("../models/anotherWork.model"); 
const Response = require("../models/response"); 

exports.getAnotherWork = function (req, res) {
  var request = new Response(req.body);
  ANOTHER.getAnotherWork(req.body.userId, req.body.customerId, request, function (err, another) {
    if (err) res.send(err);
    res.json(another);
  }); 
};
