var Customer = require("../models/customer.model");
const Response = require("../models/response"); 

exports.getCustomer = function (req, res) {
  var request = new Response(req.body);
  Customer.getAllCustomer(request, function (err, customer) {
    if (err) res.send(err);
    res.json(customer);
  });
};

exports.getCustomerID = function (req, res) {
  Customer.getCustomerID(req.params.id, function (err, customer) {
    if (err) res.send(err);
    res.json(customer);
  });
};

exports.getCustomerTel = function (req, res) {
  Customer.getCustomerTel(req.params.tel, function (err, customer) {
    if (err) res.send(err);
    res.json(customer);
  });
};

exports.getCustomerAccount = function (req, res) {
  Customer.getCustomerAccount(req.params.account, function (err, customer) {
    if (err) res.send(err);
    res.json(customer);
  });
};

exports.insertCustomer = function (req, res) {
  var newcustomer = new Customer(req.body); 
  Customer.insertCustomer(newcustomer, function (err, customer) {
    if (err) res.send(err);
    res.json(customer);
  });
};

exports.deleteCustomer = function (req, res) {
  Customer.deleteCustomer(req.params.id, function (err, customer) {
    if (err) {
      res.send(err);
    } else {
      res.json(customer);
    }
  });
};

exports.updateCustomer = function (req, res) {
  var newcustomer = new Customer(req.body); 
  Customer.updateCustomer(req.params.id, newcustomer, function (err, customer) {
    if (err) res.send(err);
    res.json(customer);
  });
};

exports.transfer = function (req, res) {
  var transfer = new Customer(req.body); 
  Customer.transfer(transfer.transferOut, transfer.amount, transfer.transferIn, function (err, transferData) {
    if (err) res.send(err);
    res.json(transferData);
  });
};

exports.getAllTransferOut = function (req, res) {
  Customer.getAllTransferOut(function (err, transferOut) {
    if (err) res.send(err);
    res.json(transferOut);
  });
};

