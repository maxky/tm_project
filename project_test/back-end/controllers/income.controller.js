var Income = require("../models/income.model");

exports.insertIncome = function (req, res) {
  var income = new Income(req.body);
  Income.insertIncome(income, function (err, income) {
    if (err) res.send(err);
    res.json(income);
  });
};
