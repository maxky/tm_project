const Noti = require("../models/noti.model");
const Response = require("../models/response");

exports.getAllNoti = function (req, res) {
  var request = new Response(req.body);
  Noti.getAllNoti(req.body.userId, request, function (err, notification) {
    if (err) res.send(err);
    res.json(notification);
  });
};

exports.updateNoti = function (req, res) {
  var request = new Response(req.body); 
  Noti.updateNoti(
    req.params.id,
    req.body.userId,
    request, 
    function (err, notification) {
      if (err) res.send(err);
      res.json(notification);
    }
  );
};
