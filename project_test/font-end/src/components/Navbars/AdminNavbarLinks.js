import React from "react";
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Hidden from "@material-ui/core/Hidden";
import Poppers from "@material-ui/core/Popper";
import AddAlert from "@material-ui/icons/AddAlert";
import Snackbar from "components/Snackbar/Snackbar.js";
// @material-ui/icons
import Power from "@material-ui/icons/PowerSettingsNew";
import Notifications from "@material-ui/icons/Notifications";
// core components
// import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import styles from "assets/jss/jdbbank/components/headerLinksStyle.js";
import Loding from "../../views/Alert/Loading";
import { compose } from "redux";
import { connect } from "react-redux";
// fetch data
import {
  getNotification,
  updateNotification,
} from "../../stores/notification/notification";

const useStyles = makeStyles(styles);

function NavbarComponent(props) {
  const classes = useStyles();
  const [openNotification, setOpenNotification] = React.useState(null);
  const [noti, setNoti] = React.useState(null);
  const [openProfile, setOpenProfile] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const notiData = props.notifications.map((item) => item);

  const handleClickNotification = (event) => {
    if (openNotification && openNotification.contains(event.target)) {
      setOpenNotification(null);
    } else {
      setOpenNotification(event.currentTarget);
    }
  };
  const handleClickProfile = (event) => {
    if (openProfile && openProfile.contains(event.target)) {
      setOpenProfile(null);
    } else {
      setOpenProfile(event.currentTarget);
    }
  };

  const openLoding = () => {
    setOpen(true);
  };

  const handleCloseNotification = () => {
    setOpenNotification(null);
  };

  const handleCloseProfile = () => {
    openLoding();
    setOpenProfile(null);
    setTimeout(() => {
      setOpen(false);
      localStorage.setItem("isLoggedIn", "false");
      localStorage.removeItem("session");
      localStorage.removeItem("token");
      localStorage.removeItem("loginid");
      window.location.assign("/auth/login");
    }, 2000);
  };

  const [tc, setTC] = React.useState(false);

  const showNotification = (place, data) => {
    // + ", " + "datetime: " + data.datetime + ", " +  "function: " + data.functionName
    setNoti(
      "reference: " +
        data.reference +
        ", datetime: " +
        data.datetime +
        ", function: " +
        data.functionName +
        ", data: " +
        data.data
    );
    setOpenNotification(null);
    if (place) {
      setTC(true);
      setTimeout(function () {
        setTC(false);
        props.updateNoti(data.id);
      }, 6000);
    }
  };
  return (
    <div>
      <div className={classes.manager}>
        <Button
          color={window.innerWidth > 959 ? "transparent" : "white"}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-owns={openNotification ? "notification-menu-list-grow" : null}
          aria-haspopup="true"
          onClick={handleClickNotification}
          className={classes.buttonLink}
        >
          <Notifications className={classes.icons} />
          {props.notifications.length > 0 ? (
            <span
              className={classes.notifications}
              disabled={props.notifications.length < 0 ? true : false}
            >
              {props.notifications.length}
            </span>
          ) : null}
          <Hidden mdUp implementation="css">
            <p onClick={handleCloseNotification} className={classes.linkText}>
              Notification
            </p>
          </Hidden>
        </Button>
        <Poppers
          open={Boolean(openNotification)}
          anchorEl={openNotification}
          transition
          disablePortal
          className={
            classNames({ [classes.popperClose]: !openNotification }) +
            " " +
            classes.popperNav
          }
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="notification-menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleCloseNotification}>
                  <MenuList role="menu">
                    {notiData.map((item, index) => (
                      <MenuItem
                        key={index}
                        onClick={() => showNotification("tc", item)}
                        className={classes.dropdownItem}
                      >
                        {item.reference}
                      </MenuItem>
                    ))}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Poppers>
      </div>
      <div className={classes.manager}>
        <Button
          color={window.innerWidth > 959 ? "transparent" : "white"}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-owns={openProfile ? "profile-menu-list-grow" : null}
          aria-haspopup="true"
          onClick={handleClickProfile}
          className={classes.buttonLink}
        >
          <Power className={classes.icons} />
        </Button>
        <Poppers
          open={Boolean(openProfile)}
          anchorEl={openProfile}
          transition
          disablePortal
          className={
            classNames({ [classes.popperClose]: !openProfile }) +
            " " +
            classes.popperNav
          }
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="profile-menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <Paper>
                <MenuList role="menu">
                  <MenuItem
                    onClick={handleCloseProfile}
                    className={classes.dropdownItem}
                  >
                    ອອກຈາກລະບົບ
                  </MenuItem>
                </MenuList>
              </Paper>
            </Grow>
          )}
        </Poppers>
        <Loding open={open} />
        {noti ? (
          <Snackbar
            place="tc"
            color="success"
            icon={AddAlert}
            message={noti}
            open={tc}
            closeNotification={() => setTC(false)}
            close
          />
        ) : null}
      </div>
    </div>
  );
}

class AdminNavbarLinks extends React.Component {
  constructor(props) {
    super(props); 
    this.updateNoti = this.updateNoti.bind(this);
  }
  // get data
  componentDidMount() {
    this.overloadData();
    setInterval(() => {
      this.overloadData();
    }, 10000);
  }

  // function overload
  overloadData() {
    this.props.dispatch(getNotification());
  }

  // function call to redux
  updateNotificate(reqId) {
   this.props.dispatch(updateNotification(reqId)); 
  }

  // function update
  updateNoti(reqId) {
    this.updateNotificate(reqId);
    this.overloadData();
  }

  render() {
    const { error, pending, notifications } = this.props;
    // store action
    if (pending) {
      return <div>Pending...!</div>;
    } else if (error) {
      return <div>Error:{error.message}</div>;
    } else {
      return (
        <div>
          <NavbarComponent
            notifications={notifications}
            updateNoti={this.updateNoti}
          />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  notifications: state.notification.notification,
  updateNotification: state.notification.updateNotification,
  pending: state.notification.pengding,
  error: state.notification.error,
});

export default compose(connect(mapStateToProps, null))(AdminNavbarLinks);
