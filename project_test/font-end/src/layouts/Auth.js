import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar   
import route from "../routes/Auth/routes"; 

const switchRoutes = (
  <Switch>
    {route.map((prop, key) => {
      if (prop.layout === "/auth") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    })}
    <Redirect from="*" to="/auth/login" />
  </Switch>
); 

export default function Auth() {
  const mainPanel = React.createRef();  
  return <div ref={mainPanel} style={{ marginTop: 50 }}><center>{switchRoutes}</center></div>;
}
