import { USER_PENDING, USER_GET, USER_ERROR, USER_GET_ID, USER_LOGOUT } from "./action";

const initialState = {
  user: [],
  userID: [],
  pending: false,
  logout: false,
  error: null,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case USER_PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case USER_GET:
      return {
        ...state,
        pending: false,
        user: action.payload.user,
      };
    case USER_GET_ID:
      return {
        ...state,
        pending: false,
        userID: action.payload.userID,
      };
    case USER_ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        user: [],
      };
    case USER_LOGOUT:
      return {
        ...state,
        logout: action.payload.userLogout
      };
    default:
      return state;
  }
}
