export const USER_PENDING = "USER_PENDING";
export const USER_GET = "USER_GET";
export const USER_GET_ID = "USER_GET_ID";
export const USER_POST_DATA = "USER_POST_DATA";
export const USER_DELETE_DATA = "USER_DELETE_DATA";
export const USER_POST_LOGIN = "USER_POST_LOGIN";
export const USER_ERROR = "USER_ERROR";
export const USER_LOGOUT = "USER_LOGOUT";

export const userPending = () => ({
  type: USER_PENDING,
});

export const userGet = (user) => ({
  type: USER_GET,
  payload: { user },
});

export const userGetId = (userID) => ({
  type: USER_GET_ID,
  payload: { userID },
});

export const userPostData = (userData) => ({
  type: USER_POST_DATA,
  payload: userData,
});

export const userDelete = (userId) => ({
  type: USER_DELETE_DATA,
  payload: { userId },
});

export const userPostLogin = (userLogin) => ({
  type: USER_POST_LOGIN,
  payload: userLogin,
});

export const userPostLogout = (userLogout) => ({
  type: USER_LOGOUT,
  payload: userLogout,
});

export const userError = (error) => ({
  type: USER_ERROR,
  payload: { error },
});
