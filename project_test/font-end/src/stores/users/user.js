import { userPending, userGet, userGetId, userPostData, userPostLogout, userDelete, userError } from "./action";
import config from "../../config/config"; 

const userPath = "user";
const postUsers = "auth";

export function getUser() {
    return dispatch => {
        dispatch(userPending());
        return fetch(config.URL_API + userPath, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: localStorage.getItem("token"),
            },
        })
        .then(handleError)
        .then(res => res.json())
        .then(result => {
            // console.log(result)
            dispatch(userGet(result))
            return result;
        })
        .catch(error => dispatch(userError(error)));
    }
}

export function getUserID(userID) {
    return dispatch => {
        dispatch(userPending());
        return fetch(config.URL_API + userPath + "/" + userID, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: localStorage.getItem("token"),
            },
        })
        .then(handleError)
        .then(res => res.json())
        .then(result => {
            // console.log(result);
            dispatch(userGetId(result[0]))
            return result;
        })
        .catch(error => dispatch(userError(error)));
    }
}

export function postUser(data) {
    return dispatch => {
        dispatch(userPending());
        return fetch(config.URL_API + postUsers + "/register" , {
            method: 'Post',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        })
        .then(handleError)
        .then(res => res.json()) 
        .then(result => {
            dispatch(userPostData(result))
        }) 
        .catch(error => dispatch(userError(error)));
    }
}

export function deleteUser(userID) {
    return dispatch => {
        dispatch(userPending());
        return fetch(config.URL_API + userPath + "/delete/" + userID , {
            method: 'Delete',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: localStorage.getItem("token"),
              },
        })
        .then(handleError)
        .then(res => res.json())  
        .then(result => {
            dispatch(userDelete(result))
        })
        .catch(error => dispatch(userError(error)));
    }
}

export function postUserLogin(data) {
    return dispatch => {
        dispatch(userPending());
        return fetch(config.URL_API + postUsers + "/login" , {
            method: 'Post',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        })
        .then(handleError)
        .then(res => res.json()) 
        // .then(result => {
        //     dispatch(userPostLogin(result))
        // }) 
        .catch(error => dispatch(userError(error)));
    }
}

export function postUserLogout(item) {
    return dispatch => {
        dispatch(userPending()); 
        dispatch(userPostLogout(item)); 
    }
}

function handleError(response) {
    if(!response.ok) {
        throw Error(response.statucText);
    }
    return response;
}

const User = {
    getUser,
    getUserID,
    postUser,
    deleteUser,
    postUserLogin
}

export default User;