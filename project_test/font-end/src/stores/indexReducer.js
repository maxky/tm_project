import { combineReducers } from "redux";
import user from "./users/reducer";
import account from "./account/reducer";
import notification from "./notification/reducer";

export default combineReducers ({
    user,
    account,
    notification
});