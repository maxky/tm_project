export const ACOUNTPENDING = "ACOUNTPENDING";
export const GET_TOTAL_ACCOUNT = "GET_TOTAL_ACCOUNT"; 
export const GET_DETAIL_INCOME = "GET_DETAIL_INCOME"; 
export const GET_DETAIL_EXPENSES = "GET_DETAIL_EXPENSES"; 
export const ACOUNTERROR = "ACOUNTERROR";

export const AccPending = () => ({
  type: ACOUNTPENDING,
}); 

export const GetTotalAccount = (TotalAccount) => ({
  type: GET_TOTAL_ACCOUNT,
  payload: { TotalAccount },
}); 

export const GetDetailIncome = (DetailIncome) => ({
  type: GET_DETAIL_INCOME,
  payload: { DetailIncome },
}); 

export const GetDetailExpenses = (DetailExpenses) => ({
  type: GET_DETAIL_EXPENSES,
  payload: { DetailExpenses },
}); 

export const AccError = (error) => ({
  type: ACOUNTERROR,
  payload: { error },
});
