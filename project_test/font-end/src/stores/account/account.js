import {
  AccPending,
  GetTotalAccount,
  GetDetailIncome,
  GetDetailExpenses,
  AccError,
} from "./action";
import config from "../../config/config";

const reportPath = "report";
const IncomePath = "income";
const ExpensesPath = "expenses";

export function getTotalAccountData(data) {
  return (dispatch) => {
    dispatch(AccPending());
    return fetch(config.URL_API + reportPath + "/total", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {
        dispatch(GetTotalAccount(result));
        return result;
      })
      .catch((error) => dispatch(AccError(error)));
  };
}

export function postIncome(data) {
  return (dispatch) => {
    dispatch(AccPending());
    return fetch(config.URL_API + IncomePath + "/insert", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {
        return result;
      })
      .catch((error) => dispatch(AccError(error)));
  };
}

export function postExpenses(data) {
  return (dispatch) => {
    dispatch(AccPending());
    return fetch(config.URL_API + ExpensesPath + "/insert", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {
        return result;
      })
      .catch((error) => dispatch(AccError(error)));
  };
}

export function DetailIncome(data) {
  return (dispatch) => {
    dispatch(AccPending());
    return fetch(config.URL_API + "report/statment/" + IncomePath, {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {
        dispatch(GetDetailIncome(result.data));
        return result;
      })
      .catch((error) => dispatch(AccError(error)));
  };
}

export function DetailExpenses(data) {
  return (dispatch) => {
    dispatch(AccPending());
    return fetch(config.URL_API + "report/statment/" + ExpensesPath, {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {
        dispatch(GetDetailExpenses(result.data));
        return result;
      })
      .catch((error) => dispatch(AccError(error)));
  };
}

function handleError(response) {
  if (!response.ok) {
    throw Error(response.statucText);
  }
  return response;
}

const User = {
  getTotalAccountData,
  postIncome,
  postExpenses,
  DetailIncome,
  DetailExpenses
};

export default User;
