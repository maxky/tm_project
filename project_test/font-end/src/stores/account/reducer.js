import {
  ACOUNTPENDING,
  GET_TOTAL_ACCOUNT,
  GET_DETAIL_INCOME,
  GET_DETAIL_EXPENSES,
  ACOUNTERROR,
} from "./action";

const initialState = {
  TotalAccount: [],
  DetailIncome: [],
  DetailExpenses: [],
  pending: false,
  error: null,
};

export default function totalAccountRed(state = initialState, action) {
  switch (action.type) {
    case ACOUNTPENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_TOTAL_ACCOUNT:
      return {
        ...state,
        pending: false,
        TotalAccount: action.payload.TotalAccount,
      };
    case GET_DETAIL_INCOME:
      return {
        ...state,
        pending: false,
        DetailIncome: action.payload.DetailIncome,
      };
    case GET_DETAIL_EXPENSES:
      return {
        ...state,
        pending: false,
        DetailExpenses: action.payload.DetailExpenses,
      };
    case ACOUNTERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}
