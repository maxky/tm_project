import {
  PENDING,
  GET_NOTIFICATION,
  UPDATE_NOTIFICATION,
  ERROR,
} from "./action";

const initialState = {
  notification: [],
  updateNotification: [],
  pending: false,
  error: null,
};

export default function notification(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_NOTIFICATION:
      return {
        ...state,
        pending: false,
        notification: action.payload.notification,
      };
    case UPDATE_NOTIFICATION:
      return {
        ...state,
        pending: false,
        updateNotification: action.payload.updateNotification,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}
