import { Pending, GetNotification, UpdateNotification, Error } from "./action";
import config from "../../config/config";

const notificationPath = "notification/";

export function getNotification() {
  return (dispatch) => {
    dispatch(Pending());
    return fetch(config.URL_API + notificationPath, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {
        dispatch(GetNotification(result.data));
        return result;
      })
      .catch((error) => dispatch(Error(error)));
  };
}

export function updateNotification(reqId) {
  return (dispatch) => {
    dispatch(Pending());
    return fetch(config.URL_API + notificationPath + "/update/" + reqId, {
      method: "put",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      },
      body: JSON.stringify({ userId: localStorage.getItem("loginid") }),
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => { 
        dispatch(UpdateNotification(result));
        return result;
      })
      .catch((error) => dispatch(Error(error)));
  };
}

function handleError(response) {
  if (!response.ok) {
    throw Error(response.statucText);
  }
  return response;
}

const notification = {
  getNotification,
  updateNotification,
};

export default notification;
