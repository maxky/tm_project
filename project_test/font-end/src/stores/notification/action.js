export const PENDING = "PENDING";
export const GET_NOTIFICATION = "GET_NOTIFICATION";
export const UPDATE_NOTIFICATION = "UPDATE_NOTIFICATION";
export const ERROR = "ERROR";

export const Pending = () => ({
  type: PENDING,
});

export const GetNotification = (notification) => ({
  type: GET_NOTIFICATION,
  payload: { notification },
});

export const UpdateNotification = (updateNotification) => ({
  type: UPDATE_NOTIFICATION,
  payload: { updateNotification },
});

export const Error = (error) => ({
  type: ERROR,
  payload: { error },
});
