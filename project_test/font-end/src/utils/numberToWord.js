//  ອ່ານເປັນໂຕໜັງສື
/*eslint-disable*/
function NumberToWords(number) {
  var words = ''
  // var wordTo_Text = new Array()
  if (number < 0 || number > 9999999999999) {
    words = 'Number Out of range'
  }
  var millions = Math.floor(number / 1000000) /// millions
  number -= millions * 1000000
  var onhundreds = Math.floor(number / 100000) //= =on hundreds Thousands
  number -= onhundreds * 100000
  var thousands = Math.floor(number / 1000) //= ==Thousands
  number -= thousands * 1000
  var hundreds = Math.floor(number / 100) // hundreds
  number -= hundreds * 100
  var ten = Math.floor(number / 10) //= ==ten
  var one = number % 10 //= ==one
  if (millions) {
    words += NumberToWords(millions) + 'ລ້ານ'
  }
  if (onhundreds) {
    words += ' ' + NumberToWords(onhundreds) + 'ແສນ' // hundreds Thousands
  }
  if (thousands) {
    words += ' ' + NumberToWords(thousands) + 'ພັນ'
  }
  if (hundreds) {
    words += ' ' + NumberToWords(hundreds) + 'ຮ້ອຍ'
  }
  var ones = new Array('', 'ໜຶ່ງ', 'ສອງ', 'ສາມ', 'ສີ່', 'ຫ້າ', 'ຫົກ', 'ເຈັດ', 'ແປດ', 'ເກົ້າ', 'ສິບ',
    'ສິບແອັດ', 'ສິບສອງ', 'ສິບສາມ', 'ສິບສີ່', 'ສິບຫ້າ', 'ສິບຫົກ', 'ສິບເຈັດ', 'ສິບແປດ', 'ສິບເກົ້າ')
  var tens = new Array('', '', 'ຊາວ', 'ສາມສິບ', 'ສີ່ສິບ', 'ຫ້າສິບ', 'ຫົກສິບ',
    'ເຈັດສິບ', 'ແປດສິບ', 'ເກົ້າສິບ')

  if (ten || one) {
    if (words != null) {
      words += ''
    }
    if (ten < 2) {
      words += ones[ten * 10 + one] + ''
    } else {
      words += tens[ten] + ''
      if (one) {
        if (one == 1) {
          words += 'ແອັດ' // ເອັດ
        } else {
          words += '' + ones[one] + ''
        }
      }
    }
  }
  if (words == '' || words == 0) {
    words = 'ສູນ'
  }
  // words  = words.substring(0, words.length - 1);
  // wordTo_Text.push(words);
  return words
}

function withDecimal(n) {
  if (n) {
    var nums = n.toString().split('.')
    var whole = NumberToWords(nums[0])
    if (nums.length == 2) {
      var fraction = NumberToWords(nums[1])
      if (fraction == '') {
        return whole
      } else {
        if (whole !== '') {
          return whole + ' ຈຸດ ' + fraction
        } else {
          return 'ສູນຈຸດ ' + fraction
        }
      }
    } else {
      return whole
    }
  }
}
export default withDecimal;
