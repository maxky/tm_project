// ----------- Bill Number -----------
const billNumberTop = {
  position: "absolute",
  top: "2%",
  left: "83%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};
const billNumberBelow = {
  position: "absolute",
  top: "52.8%",
  left: "83%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};

// ----------- date -----------
const dateTop = {
  position: "absolute",
  top: "3.8%",
  left: "76%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};
const dateBelow = {
  position: "absolute",
  top: "54.7%",
  left: "76%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};

// ----------- Bill request -----------
const reqNumberTop = {
  position: "absolute",
  top: "13.68%",
  left: "19%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};
const reqNumberBelow = {
  position: "absolute",
  top: "64.45%",
  left: "19%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};

// ----------- tel -----------
const telTop = {
  position: "absolute",
  top: "13.68%",
  left: "65%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};
const telBelow = {
  position: "absolute",
  top: "64.45%",
  left: "65%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};

// ----------- account -----------
const accountTop = {
  position: "absolute",
  top: "16%",
  left: "20%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};
const accountBelow = {
  position: "absolute",
  top: "66.85%",
  left: "20%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};

// ----------- customer name -----------
const customerNameTop = {
  position: "absolute",
  top: "16%",
  left: "54%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Phetsarath OT",
};
const customerNameBelow = {
  position: "absolute",
  top: "66.85%",
  left: "54%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Phetsarath OT",
};

// ----------- total -----------
const totalTop = {
  position: "absolute",
  top: "22%",
  left: "22%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};
const totalBelow = {
  position: "absolute",
  top: "72.85%",
  left: "22%",
  fontWeight: "bold",
  fontSize: "12pt",
  fontFamily: "Times New Roman",
};

// ----------- currency -----------
const currencyTop = {
    position: "absolute",
    top: "22%",
    left: "56%",
    fontWeight: "bold",
    fontSize: "12pt",
    fontFamily: "Times New Roman",
}
const currencyBelow = {
    position: "absolute",
    top: "72.85%",
    left: "56%",
    fontWeight: "bold",
    fontSize: "12pt",
    fontFamily: "Times New Roman",
}

// ----------- total letter -----------
const totalLetterTop = {
    position: "absolute",
    top: "24.45%",
    left: "24%",
    fontWeight: "bold",
    fontSize: "12pt",
    fontFamily: "Phetsarath OT",
}
const totalLetterBelow = {
    position: "absolute",
    top: "75.2%",
    left: "24%",
    fontWeight: "bold",
    fontSize: "12pt",
    fontFamily: "Phetsarath OT",
}

// ----------- currency -----------
const currencyLetterTop = {
    position: "absolute",
   top: "24.45%",
    left: "60%",
    fontWeight: "bold",
    fontSize: "12pt",
    fontFamily: "Phetsarath OT",
}
const currencyLetterBelow = {
    position: "absolute",
    top: "75.2%",
    left: "60%",
    fontWeight: "bold",
    fontSize: "12pt",
    fontFamily: "Phetsarath OT",
}

// ----------- Detail -----------
const detailTop = {
    position: "absolute",
    top: "30.4%",
    left: "10%",
    fontWeight: "bold",
    fontSize: "12pt"
}
const detailBelow = {
    position: "absolute",
    top: "81.2%",
    left: "10%",
    fontWeight: "bold",
    fontSize: "12pt"
}
// ----------- export -----------
const styles = {
  billNumberTop,
  billNumberBelow,
  dateTop,
  dateBelow,
  reqNumberTop,
  reqNumberBelow,
  telTop,
  telBelow,
  accountTop,
  accountBelow,
  customerNameTop,
  customerNameBelow,
  totalTop,
  totalBelow,
  currencyTop,
  currencyBelow,
  totalLetterTop,
  totalLetterBelow,
  currencyLetterTop,
  currencyLetterBelow,
  detailTop,
  detailBelow
};
export default styles;
