 
// core components/views for Admin layout  
import Login from "views/Login/Login";

const dashboardRoute = [  
  {
    path: "/login",  
    component: Login,
    layout: "/auth"
  }
];

export default dashboardRoute;
