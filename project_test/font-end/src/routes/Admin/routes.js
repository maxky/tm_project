// @material-ui/icons 
import Transfer from "@material-ui/icons/Payment";  
import AddCircleOutline from "@material-ui/icons/AddCircleOutline";  
import ShoppingCart from "@material-ui/icons/ShoppingCart"; 
// core components/views for Admin layout 
import Report from "views/Report/Main"; 
import Income from "views/Income/Main"; 
import Expenses from "views/Expenses/Main"; 

const dashboardRoutes = [ 
  {
    path: "/report",
    name: "ສະຫຼຸບລາຍຮັບ-ຈ່າຍ", 
    icon: Transfer,
    component: Report,
    layout: "/admin"
  },  
  {
    path: "/income",
    name: "ລາຍຮັບ", 
    icon: AddCircleOutline,
    component: Income,
    layout: "/admin"
  }, 
  {
    path: "/expenses",
    name: "ລາຍຈ່າຍ", 
    icon: ShoppingCart,
    component: Expenses,
    layout: "/admin"
  }, 
];

export default dashboardRoutes;
