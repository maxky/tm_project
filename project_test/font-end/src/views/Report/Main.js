import React from "react";
// @material-ui/core components
import Search from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";
// core components
import { withStyles } from "@material-ui/core/styles";
import "../../assets/css/style.css";
import Loding from "../Alert/Loading";
import InfoAlert from "../Alert/Info";
import Danger from "../Alert/Danger";
import ComponentIncome from "../Alert/ComponentIncome";
import ComponentExpenses from "../Alert/ComponentExpenses";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import { connect } from "react-redux";
import { compose } from "redux";
import moment from "moment";
// search data
import {
  getTotalAccountData,
  DetailIncome,
  DetailExpenses,
} from "../../stores/account/account";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "Phetsarath OT",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  frontName: {
    fontFamily: "Phetsarath OT",
    fontSize: "18px",
  },
  frontNames: {
    fontFamily: "Phetsarath OT",
  },
  frontNamess: {
    fontFamily: "Phetsarath OT",
    color: "blue",
  },
  textField: {
    width: "300px",
  },
};

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      showData: false,
      openEmpty: false,
      openError: false,
      openComponent: false,
      openComponentIncome: false,
      openComponentExpenses: false,
      startdate: moment().locale("en").format("YYYY-MM-DD"),
      enddate: moment().locale("en").format("YYYY-MM-DD"),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  // function poppup card empty
  HandleOpenEmpty = () => {
    this.setState({ openEmpty: true });
  };

  // function poppup card openComponent
  HandleopenComponentIncome = () => {
    this.setState({ openComponentIncome: true });
  };

  // function poppup card openComponent
  HandleopenComponentExpenses = () => {
    this.setState({ openComponentExpenses: true });
  };

  // function poppup card error
  HandleOpenError = () => {
    this.setState({ openError: true });
  };

  // function close card empty and error
  handleClose = () => {
    this.setState({ openEmpty: false });
    this.setState({ openError: false });
    this.setState({ openComponentIncome: false });
    this.setState({ openComponentExpenses: false });
  };

  // function ການຄົ້ນຫາຂໍ້ມູນ
  search = () => {
    if (this.state.startdate === "" || this.state.enddate === "") {
      this.HandleOpenEmpty();
    } else {
      this.setState({ loading: true });
      this.setState({ showData: false });
      setTimeout(() => {
        this.setState({ loading: false });
        const data = {
          starttime: this.state.startdate,
          endtime: this.state.enddate,
          id: localStorage.getItem("loginid"),
        };
        this.props.dispatch(getTotalAccountData(data));
        this.props.dispatch(DetailIncome(data));
        this.props.dispatch(DetailExpenses(data));
        this.setState({ showData: true });
      }, 2000);
    }
  };

  openDeatilIncome = () => {
    this.HandleopenComponentIncome();
  };

  openDeatilExpenses = () => {
    this.HandleopenComponentExpenses();
  };

  render() {
    const { loading, startdate, enddate, showData } = this.state;
    const { classes, error, pending, TotalAccount } = this.props;
    const priceSplitter = (number) =>
      number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // store action
    if (pending) {
      return <div>Pending...!</div>;
    } else if (error) {
      return <div>Error:{error.message}</div>;
    } else {
      return (
        <>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Card>
                <CardHeader color="danger">
                  <h4 className={classes.cardTitleWhite}>ຄົ້ນຫາຂໍ້ມູນ</h4>
                </CardHeader>
                <CardBody>
                  <GridContainer>
                    <GridItem xs={12} md={4}>
                      <label className={classes.frontName}>ແຕ່ວັນທີ</label>
                      <TextField
                        id="date"
                        type="date"
                        className={classes.textField}
                        name="startdate"
                        onChange={this.handleChange}
                        value={startdate}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={4}>
                      <label className={classes.frontName}>ຫາວັນທີ</label>
                      <TextField
                        id="date"
                        type="date"
                        className={classes.textField}
                        name="enddate"
                        onChange={this.handleChange}
                        value={enddate}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={4}>
                      <Button
                        color="white"
                        aria-label="edit"
                        justIcon
                        round
                        onClick={this.search}
                      >
                        <Search />
                      </Button>
                    </GridItem>
                  </GridContainer>
                </CardBody>
              </Card>
            </GridItem>
          </GridContainer>
          {showData === true ? (
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <Card>
                  <CardHeader color="danger">
                    <h4 className={classes.cardTitleWhite}>ສະຫຼຸບ</h4>
                  </CardHeader>
                  <CardBody>
                    <GridContainer>
                      <GridItem xs={12} md={4}>
                        <label className={classes.frontName}>ລາຍຮັບ</label>
                        <h3
                          className={classes.frontNames}
                          style={{ color: "green" }}
                        >
                          {TotalAccount.income === null
                            ? "ບໍ່ມີລາຍຮັບ"
                            : priceSplitter(TotalAccount.income) + " ກີບ"}
                        </h3>
                        {TotalAccount.income !== null ? (
                          <a
                            className={classes.frontNamess}
                            href="#income"
                            onClick={this.openDeatilIncome}
                          >
                            ລາຍລະອຽດ
                          </a>
                        ) : null}
                      </GridItem>
                      <GridItem xs={12} sm={12} md={4}>
                        <label className={classes.frontName}>ລາຍຈ່າຍ</label>
                        <h3
                          className={classes.frontNames}
                          style={{ color: "red" }}
                        >
                          {TotalAccount.expenses === null
                            ? "ບໍ່ມີລາຍຈ່າຍ"
                            : priceSplitter(TotalAccount.expenses) + " ກີບ"}
                        </h3>
                        {TotalAccount.expenses !== null ? (
                          <a
                            className={classes.frontNamess}
                            href="#expenses"
                            onClick={this.openDeatilExpenses}
                          >
                            ລາຍລະອຽດ
                          </a>
                        ) : null}
                      </GridItem>
                      <GridItem xs={12} sm={12} md={4}>
                        <label className={classes.frontName}>ຍອດເຫຼືອ</label>
                        {TotalAccount.total > 0 ? (
                          <h3
                            className={classes.frontNames}
                            style={{ color: "green" }}
                          >
                            {priceSplitter(TotalAccount.total)} ກີບ
                          </h3>
                        ) : TotalAccount.total < 0 ? (
                          <h3
                            className={classes.frontNames}
                            style={{ color: "red" }}
                          >
                            {priceSplitter(TotalAccount.total)} ກີບ
                          </h3>
                        ) : (
                          <h3
                            className={classes.frontNames}
                            style={{ color: "blue" }}
                          >
                            ບໍ່ມີຍອດເງີນ
                          </h3>
                        )}
                      </GridItem>
                    </GridContainer>
                  </CardBody>
                </Card>
              </GridItem>
            </GridContainer>
          ) : null}

          <Loding open={loading} />
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຄ່າວ່າງ */}
          <InfoAlert
            open={this.state.openEmpty}
            handleClose={this.handleClose}
            message="ວັນທີໃຫ້ຄົບ"
          />
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຖືກຜິດ */}
          <Danger
            open={this.state.openError}
            handleClose={this.handleClose}
            message={error}
          />
          {/* ເປີດ component ລາຍລະອຽດຂໍ້ມູນ */}
          <ComponentIncome
            open={this.state.openComponentIncome}
            handleClose={this.handleClose}
          />
          {/* ເປີດ component ລາຍລະອຽດຂໍ້ມູນ */}
          <ComponentExpenses
            open={this.state.openComponentExpenses}
            handleClose={this.handleClose}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  TotalAccount: state.account.TotalAccount,
  pending: state.account.pengding,
  error: state.account.error,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Main);
