import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import Dialog from "@material-ui/core/Dialog";
import Paper from "@material-ui/core/Paper";
import {
  DialogContent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@material-ui/core";
import { connect } from "react-redux";

const styles = {
  textField: {
    width: "300px",
  },
  frontName: {
    fontFamily: "Phetsarath OT",
  },
  table: {
    minWidth: 650,
  },
};

class ComponentIncome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // next page
      page: 0,
      rowsPerPage: 5,
    };
  }

  // next page
  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: +event.target.value });
    this.setState({ page: 0 });
  };

  render() {
    const {
      open,
      handleClose,
      classes,
      DetailIncome, 
      pending,
      error, 
    } = this.props;
    const { page, rowsPerPage } = this.state;
    const priceSplitter = (number) =>
      number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
    let status = "ລາຍຮັບ"; 
    // store action
    if (pending) {
      return <div>Pending...!</div>;
    } else if (error) {
      return <div>Error:{error.message}</div>;
    } else if (DetailIncome) {
      return (
        <>
          <div>
            <Dialog
              fullWidth={true}
              maxWidth={"md"}
              open={open}
              onClose={handleClose}
              aria-labelledby="max-width-dialog-title"
            >
              <DialogContent>
                <label style={{ fontFamily: "Phetsarath OT" }}>{status}</label>
                <TableContainer component={Paper}>
                  <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell style={{ fontFamily: "Phetsarath OT" }}>
                          ລຳດັບ
                        </TableCell>
                        <TableCell style={{ fontFamily: "Phetsarath OT" }}>
                          ຈຳນວນເງີນ
                        </TableCell>
                        <TableCell style={{ fontFamily: "Phetsarath OT" }}>
                          ລາຍລະອຽດ
                        </TableCell>
                        <TableCell style={{ fontFamily: "Phetsarath OT" }}>
                          ວັນທີ
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {DetailIncome
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map(function (row, index) {
                          return (
                            <TableRow
                              hover
                              role="checkbox"
                              tabIndex={-1}
                              key={row.id}
                            >
                              <TableCell component="th" scope="row">
                                {index + 1}
                              </TableCell>
                              <TableCell>{priceSplitter(row.amount)}</TableCell>
                              <TableCell
                                style={{ fontFamily: "Phetsarath OT" }}
                              >
                                {row.description}
                              </TableCell>
                              <TableCell>
                                {new Date(row.datetime).toLocaleDateString(
                                  "sq-AL",
                                  {
                                    year: "numeric",
                                    month: "2-digit",
                                    day: "2-digit",
                                  }
                                )}
                              </TableCell>
                            </TableRow>
                          );
                        })}
                    </TableBody>
                  </Table>
                </TableContainer>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component="div"
                  count={DetailIncome.length}
                  rowsPerPage={rowsPerPage}
                  page={page > 0 && DetailIncome.length < rowsPerPage ? 0 : page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
              </DialogContent>
            </Dialog>
          </div>
        </>
      );
    }
  }
}
const mapStateToProps = (state) => ({
  DetailIncome: state.account.DetailIncome, 
  pending: state.account.pengding,
  error: state.account.error,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(ComponentIncome);
