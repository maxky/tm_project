import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import NotInterested from "@material-ui/icons/NotInterested";
import NewReleases from "@material-ui/icons/NewReleases";
import { withStyles } from "@material-ui/core/styles";
import Loding from "./Loading";

const styles = {
  frontName: {
    fontFamily: "Phetsarath OT",
  },
  backgroundBtnBlue: {
    backgroundColor: "blue",
    color: "white",
    fontFamily: "Phetsarath OT",
  },
  backgroundBtnRed: {
    backgroundColor: "red",
    color: "white",
    fontFamily: "Phetsarath OT",
  },
};

class Cancel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  // function poppup card loding
  HandleLoding = () => {
    this.setState({ loading: true });
  };

  onClickClose = () => {
    this.HandleLoding();
    setTimeout(() => {
      window.location.reload(false);
      this.setState({ loading: false });
    }, 2000);
  };

  render() {
    const { classes } = this.props;
    return (
      <Dialog
        open={this.props.open}
        // onClose={this.props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent className={classes.frontName}>
          <center>
            <p>ແຈ້ງເຕືອນ</p>
          </center>
          <b>ຍົກເລິກລາຍການໂອນເງິນ</b>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={this.onClickClose}
            startIcon={<NotInterested />}
            className={classes.backgroundBtnRed}
          >
            ຍົກເລິກ
          </Button>
          <Button
            variant="contained"
            onClick={this.props.handleClose}
            startIcon={<NewReleases />}
            className={classes.backgroundBtnBlue}
          >
            ບໍ່ຍົກເລິກ
          </Button>
        </DialogActions>
        {/* ເປີດ component loding */}
        <Loding open={this.state.loading} />
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Cancel);
