import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Refresh from "@material-ui/icons/Refresh";
import { withStyles } from "@material-ui/core/styles";
const styles = {
  frontName: {
    fontFamily: "Phetsarath OT",
  },
  backgroundBtn: {
    backgroundColor: "blue",
    color: "white",
  },
  fontColorBulee: {
    color: "blue",
  },
  fontColorRed: {
    color: "red",
  },
};

class Error extends React.Component {
  render() {
    const {
      classes,
      customerTel,
      customerAccount,
      LoginError,
      MoreAmount,
    } = this.props;
    let errorMessage = "";
    // ກວດສອບ error ຈາກການຄົ້ນຫາເລກໂທລະສັບ
    if (customerTel) {
      errorMessage = customerTel.message;
    }
    // ກວດສອບ error ຈາກການຄົ້ນຫາເລກບັນຊີ
    if (customerAccount) {
      errorMessage = customerAccount.message;
    }
    // ກວດສອບ error ຈາກການເຂົ້າສູ່່ລະບົບ
    if (LoginError) {
      errorMessage = LoginError;
    }
    // ກວດສອບ error ຈາກການປ້ອນຈພນວນເງິນທີ່ຫຼາຍກວ່າກຳນົດ
    if (MoreAmount) {
      errorMessage = MoreAmount;
    }
    return (
      <Dialog
        open={this.props.open}
        // onClose={this.props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent className={classes.frontName}>
          <center>
            <p className={classes.fontColorBulee}>ແຈ້ງເຕືອນ</p>
            <blockquote>
              <p className="blockquote blockquote-primary">
                <b className={classes.fontColorRed}>{errorMessage}</b>
                <br />
                <b className={classes.fontColorRed}>ກະລຸນາປ້ອນຂໍ້ມູນຄືນໃໝ່</b>
              </p>
            </blockquote>
          </center>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={this.props.handleClose}
            startIcon={<Refresh />}
            className={classes.backgroundBtn}
          >
            ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Error);
