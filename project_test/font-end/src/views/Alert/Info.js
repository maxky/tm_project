import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

class Info extends React.Component {
  render() {
    const { open, message, handleClose } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            info
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleClose}
            onCancel={handleClose}
            showConfirm={false} 
          >
            <p style={{ fontFamily: "Saysettha OT" }}><b>ກະລຸນາປ້ອນ {message}</b></p>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default Info;