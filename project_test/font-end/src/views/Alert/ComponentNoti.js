import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
import Button from "components/CustomButtons/Button.js";
// @material-ui/core components
import Cancel from "@material-ui/icons/Cancel";
import SaveIcon from "@material-ui/icons/Save";

class ComponentNoti extends React.Component {
  render() {
    const { open, message, handleClose, confirmData, loading } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            info
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleClose} 
            showConfirm={false}
          >
            <p style={{ fontFamily: "Saysettha OT" }}>
              <b>ບັນທຶກຂໍ້ມູນ{message}</b>
            </p>
            <Button
              variant="contained"
              color="danger"
              style={{ fontFamily: "Phetsarath OT", fontSize: "18px" }}
              startIcon={<Cancel />}
              onClick={handleClose}
            >
              ຍົກເລິກ
            </Button>
            &nbsp; &nbsp; &nbsp;
            <Button
              variant="contained"
              color="primary"
              style={{ fontFamily: "Phetsarath OT", fontSize: "18px" }}
              startIcon={<SaveIcon />}
              onClick={confirmData}
              disabled={loading} 
            >
              ບັນທຶກ
            </Button>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default ComponentNoti;
