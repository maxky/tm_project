import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent"; 
import TouchApp from "@material-ui/icons/TouchApp";

const styles = {
  frontName: {
    fontFamily: "Phetsarath OT",
  },
  backgroundBtn: {
    backgroundColor: "blue",
    color: "white",
  },
};

class Empty extends React.Component {
  render() {
    const { classes, text, logintext } = this.props;
    let emptyMessage = "";
    // ກວດສອບ empty ຄ່າວ່າງທົ່ວໄປ
    if (text) {
      emptyMessage = text;
    }
    // ກວດສອບ empty ຄ່າວ່າງເຂົ້າສູ່ລະບົບ
    if (logintext) {
      emptyMessage = logintext;
    } 
    return (
      <Dialog
        open={this.props.open}
        // onClose={this.props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent className={classes.frontName}>
          <center>
            <p>ແຈ້ງເຕືອນ</p>
          </center> 
          ກະລຸນາປ້ອນ&nbsp;<b>{emptyMessage}</b>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={this.props.handleClose}
            startIcon={<TouchApp />}
            className={classes.backgroundBtn}
          >
            ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Empty);
 
