import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent"; 
import TouchApp from "@material-ui/icons/TouchApp";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  frontName: {
    fontFamily: "Phetsarath OT",
  },
  backgroundBtn: {
    backgroundColor: "blue",
    color: "white",
  },
};

class Logout extends React.Component {
  render() {
    const { classes, text } = this.props; 
    return (
      <Dialog
        open={this.props.open}
        // onClose={this.props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent className={classes.frontName}>
          <center>
            <p>ແຈ້ງເຕືອນ</p>
          </center>
          Token ໝົດອາຍຸການໃຊ້ງານ
          <br />
          ກະລຸນາ&nbsp;<b>{text}</b>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={this.props.handleClose}
            startIcon={<TouchApp />}
            className={classes.backgroundBtn}
          >
            ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Logout);
 
