import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
import CheckCircle from "@material-ui/icons/CheckCircle";
import Button from "components/CustomButtons/Button.js";

class Confirm extends React.Component {
  render() {
    const { open, message, amount, detail, datetime, handleClose } = this.props;
    const priceSplitter = (number) =>
      number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            success
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleClose} 
            showConfirm={false}
          >
            <p style={{ fontFamily: "Saysettha OT" }}>
              <b>{message}</b>
            </p>
            <p style={{ fontFamily: "Saysettha OT" }}>
              <b>ຈຳນວນເງິນ: {priceSplitter(amount)} ກີບ</b>
            </p>
            <p style={{ fontFamily: "Saysettha OT" }}>
              <b>ລາຍລະອຽດ: {detail}</b>
            </p>
            <p style={{ fontFamily: "Saysettha OT" }}>
              <b>ວັນທີ: {datetime}</b>
            </p>
            <Button
              variant="contained"
              color="success"
              style={{ fontFamily: "Phetsarath OT", fontSize: "18px" }}
              startIcon={<CheckCircle />}
              onClick={handleClose}
            >
              ບັນທຶກ
            </Button>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default Confirm;
