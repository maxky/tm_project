import React from "react";
// @material-ui/core components
import SaveIcon from "@material-ui/icons/Save";
import TextField from "@material-ui/core/TextField";
// core components
import { withStyles } from "@material-ui/core/styles";
import "../../assets/css/style.css";
import Loding from "../Alert/Loading";
import InfoAlert from "../Alert/Info";
import Danger from "../Alert/Danger";
import Confirm from "../Alert/Confirm";
import ComponentNoti from "../Alert/ComponentNoti";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import { connect } from "react-redux";
import { compose } from "redux";
// search data
import { postExpenses } from "../../stores/account/account";
import accounting from "accounting";
import { number } from "prop-types";
import moment from "moment";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "Phetsarath OT",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  frontName: {
    fontFamily: "Phetsarath OT",
    fontSize: "18px",
  },
  frontNames: {
    fontFamily: "Phetsarath OT",
  },
  textField: {
    width: "300px",
  },
};

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      openEmpty: false,
      openError: false,
      openConfirm: false,
      openNoti: false,
      amount: number,
      detail: "",
      datetime: moment(new Date()).locale("en").format("YYYY-MM-DD"),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  // this is comment for handleChange function
  handleChangeAmount(event) {
    event.target.value = accounting.unformat(event.target.value);
    this.setState({ amount: event.target.value });
  }

  getValue() {
    return accounting.formatNumber(this.state.amount);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  // function poppup card openNoti
  openNoti = () => {
    this.setState({ openNoti: true });
  };

  // function poppup card confirm
  openConfirm = () => {
    this.setState({ openConfirm: true });
  };

  // function poppup card empty
  HandleOpenEmpty = () => {
    this.setState({ openEmpty: true });
  };

  // function poppup card error
  HandleOpenError = () => {
    this.setState({ openError: true });
  };

  // function close card empty and error
  handleClose = () => {
    this.setState({ openEmpty: false });
    this.setState({ openError: false });
    this.setState({ openConfirm: false });
    this.setState({ openNoti: false });
    this.setState({ amount: "" });
    this.setState({ detail: "" }); 
  };

  // function ການຄົ້ນຫາຂໍ້ມູນ
  search = () => {
    if (this.state.amount === 0 || this.state.detail === "") {
      this.HandleOpenEmpty();
    } else {
      this.openNoti();
    }
  };

  // function ຍີນຍັນການບັນທຶກ
  confirmData = () => {
    this.setState({ openNoti: false });
    this.setState({ loading: true });
    const data = {
      amount: this.state.amount,
      description: this.state.detail,
      datetime: this.state.datetime,
      id: localStorage.getItem("loginid"),
    };
    setTimeout(() => {
      this.props.dispatch(postExpenses(data)).then((result) => {
        this.setState({ loading: false });
        if (result.success === true) {
          this.openConfirm();
        } else if (result.success === false) {
          this.OpenError();
        }
      });
    }, 3000);
  };

  render() {
    const { loading, detail, datetime } = this.state;
    const { classes, error, pending } = this.props;
    // store action
    if (pending) {
      return <div>Pending...!</div>;
    } else if (error) {
      return <div>Error:{error.message}</div>;
    } else {
      return (
        <>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Card>
                <CardHeader color="danger">
                  <h4 className={classes.cardTitleWhite}>
                    ບັນທຶກຂໍ້ມູນລາຍຈ່າຍ
                  </h4>
                </CardHeader>
                <CardBody>
                  <GridContainer>
                    <GridItem xs={12} md={4}>
                      <label className={classes.frontName}>ຈຳນວນເງິນ</label>
                      <TextField
                        className={classes.textField}
                        name="amount"
                        value={this.getValue()}
                        onChange={(event) => this.handleChangeAmount(event)}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={4}>
                      <label className={classes.frontName}>ລາຍລະອຽດ</label>
                      <TextField
                        className={classes.textField}
                        name="detail"
                        onChange={this.handleChange}
                        value={detail}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={4}>
                      <label className={classes.frontName}>ວັນທີ</label>
                      <TextField
                        id="date"
                        type="date"
                        className={classes.textField}
                        name="datetime"
                        onChange={this.handleChange}
                        value={datetime}
                      />
                    </GridItem>
                  </GridContainer>
                  <br />
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <Button
                        variant="contained"
                        color="danger"
                        className={classes.frontName}
                        startIcon={<SaveIcon />}
                        onClick={this.search}
                      >
                        ບັນທຶກ
                      </Button>
                    </GridItem>
                  </GridContainer>
                </CardBody>
              </Card>
            </GridItem>
          </GridContainer>

          <Loding open={loading} />
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຄ່າວ່າງ */}
          <InfoAlert
            open={this.state.openEmpty}
            handleClose={this.handleClose}
            message="ຂໍ້ມູນໃຫ້ຄົບ"
          />
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຖືກຜິດ */}
          <Danger
            open={this.state.openError}
            handleClose={this.handleClose}
            message={error}
          />
          {/* ເປີດ component ຢືນຢັນການໂອນ */}
          <Confirm
            open={this.state.openConfirm}
            handleClose={this.handleClose}
            message="ບັນທຶກລາຍຈ່າຍ"
            amount={this.state.amount}
            detail={this.state.detail}
            datetime={this.state.datetime}
          />
          {/* ເປີດ component ການແຈ້ງເຕືອນການບັນທຶກ */}
          <ComponentNoti
            open={this.state.openNoti}
            handleClose={this.handleClose}
            confirmData={this.confirmData}
            message="ລາຍຈ່າຍ"
            loading={loading}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  TotalAccount: state.account.TotalAccount,
  pending: state.account.pengding,
  error: state.account.error,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Main);
