import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import LockOpen from "@material-ui/icons/LockOpen";
import Lock from "@material-ui/icons/Lock";
import AccountCircle from "@material-ui/icons/AccountCircle";
import "../../assets/css/style.css";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent"; 
import Typography from "@material-ui/core/Typography";
import CustomInput from "components/CustomInput/CustomInput";
import { postUserLogin } from "../../stores/users/user";
import Loding from "../Alert/Loading";
// import Error from "../Alert/Error";
// import Empty from "../Alert/Empty";
import Danger from "../Alert/Danger";
import InfoAlert from "../Alert/Info";

const styles = {
  root: {
    maxWidth: 345,
  },
  frontPhetsarath: {
    fontFamily: "Phetsarath OT",
  },
  marginButtom: {
    marginTop: "20px",
    marginBottom: "20px",
  },
};

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loading: false,
      openEmpty: false,
      openError: false,
    };
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.loginSubmit = this.loginSubmit.bind(this);
  }

  // function poppup card empty
  HandleOpenEmpty = () => {
    this.setState({ openEmpty: true });
  };

  // function poppup card error
  HandleOpenError = () => {
    this.setState({ openError: true });
  };

  // function close card empty and error
  handleClose = () => {
    this.setState({ openEmpty: false });
    this.setState({ openError: false });
  };

  // function poppup card loding
  HandleLoding = () => {
    this.setState({ loading: true });
  };

  // this is comment for handleChange function
  handleChangeUsername(event) {
    this.setState({
      username: event.target.value,
    });
  }

  handleChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  componentDidMount() {
    if (localStorage.getItem("isLoggedIn") === "true") {
      this.props.history.push("/admin/report");
    } else {
      this.props.history.push("/auth/login");
    }
  }

  // this is comment for login function
  loginSubmit(event) {
    event.preventDefault();
    if (this.state.username === "" || this.state.password === "") {
      this.HandleOpenEmpty();
    } else {
      this.HandleLoding();
      const data = {
        username: this.state.username,
        password: this.state.password,
      };
      this.props.dispatch(postUserLogin(data)).then((result) => {
        setTimeout(() => {
          this.setState({ loading: false });
          if (result.loginID !== undefined) { 
            localStorage.setItem("isLoggedIn", "true");
            localStorage.setItem("session", result);
            localStorage.setItem("loginid", result.loginID);
            localStorage.setItem("token", "Bearer " + result.token);
            localStorage.setItem("role", result.profile);
            this.props.history.push("/admin/report");
          } else {
            this.HandleOpenError();
          }
        }, 2000);
      });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card className={classes.root}>
          <CardActionArea> 
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                className={classes.frontPhetsarath}
              >
                ເກັບລາຍຮັບ-ຈ່າຍ UAT
              </Typography>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText=""
                    id="username"
                    name="username"
                    icon={<AccountCircle />}
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      type: "text",
                      onChange: (event) => this.handleChangeUsername(event),
                      value: `${this.state.username}`,
                      placeholder: "username",
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    id="password"
                    icon={<Lock />}
                    labelText=""
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      type: "password",
                      onChange: (event) => this.handleChangePassword(event),
                      value: `${this.state.password}`,
                      placeholder: "password",
                    }}
                  />
                </GridItem>
              </GridContainer>
            </CardContent>
          </CardActionArea>
          <center>
            {" "}
            <Button
              color="danger"
              startIcon={<LockOpen />}
              style={styles.frontPhetsarath}
              onClick={this.loginSubmit}
              round
              className={classes.marginButtom}
            >
              ເຂົ້າສູ່ລະບົບ
            </Button>
          </center>
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຄ່າວ່າງ */}
          {/* <Empty
            open={this.state.openEmpty}
            handleClose={this.handleClose}
            logintext="ຊື່ຜູ້ເຂົ້າໃຊ້ ແລະ ລະຫັດຜ່ານ"
          /> */}
          {/* ເປີດ component loding */}
          <Loding open={this.state.loading} />
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຖືກຜິດ */}
          {/* <Error
            open={this.state.openError}
            handleClose={this.handleClose}
            LoginError="ຊື່ຜູ້ເຂົ້າໃຊ້ ຫຼື ລະຫັດບໍ່ຖືກຕ້ອງ"
          /> */}
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ຖືກຜິດ */}
          <Danger
            open={this.state.openError}
            handleClose={this.handleClose}
            message="ຊື່ຜູ້ເຂົ້າໃຊ້ ຫຼື ລະຫັດບໍ່ຖືກຕ້ອງ"
          /> 
          {/* ເປີດ component ກວດສອບຂໍ້ມູນທີ່ທີ່ເປັນໂຕເລກ */} 
          <InfoAlert
            open={this.state.openEmpty}
            handleClose={this.handleClose}
            message="ຊື່ຜູ້ເຂົ້າໃຊ້ ແລະ ລະຫັດຜ່ານ"
          /> 
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userPostLogin: state.user.userPostLogin,
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, null)
)(Login);
